from flask import Flask, request, render_template_string
import redis
from jinja2 import escape

app = Flask(__name__)

TEMPLATE = '''
<!DOCTYPE html>
<html>
<head>
    <title>Test Redis</title>
</head>
<body>
    <h1>Tester la Connexion à Redis</h1>
    <form method="POST">
        Nom du serveur Redis: <input type="text" name="redis_server">
        <input type="submit" value="Tester">
    </form>
    {% if message %}
        <p style="color: {{ color }}">{{ message }}</p>
    {% endif %}
</body>
</html>
'''

@app.route('/', methods=['GET', 'POST'])
def index():
    message, color = None, 'black'
    if request.method == 'POST':
        redis_server = request.form['redis_server']
        try:
            r = redis.Redis(host=redis_server, port=6379, socket_timeout=5)
            r.ping()
            message = "Je suis connecté à Redis : " + request.form['redis_server']
            color = "green"
        except redis.ConnectionError:
            message = "Connexion à Redis impossible : " + request.form['redis_server']
            color = "red"
        except redis.TimeoutError:
            message = "La connexion à Redis a expiré"
            color = "red"
    return render_template_string(TEMPLATE, message=message, color=color)

if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0')

