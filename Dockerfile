FROM python:3.8-slim as base

WORKDIR /app

COPY requirements.txt requirements.txt
RUN pip install -r requirements.txt

COPY app.py app.py

FROM base as prod

CMD ["python", "app.py"]
EXPOSE 5000

FROM base as dev

RUN pip install pytest

CMD ["python", "app.py"]
EXPOSE 5000
